Journal.Views.PostShow = Backbone.View.extend({
  initialize: function () {

  },

  render: function () {
    console.log("RENDER");
    console.log(this.post);
    var renderedContent = JST["post/show"]({ /// this gets compiled to html
      post: this.model
    });

    this.$el.html(renderedContent);
    return this;
  }
});