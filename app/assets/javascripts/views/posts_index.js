Journal.Views.PostsIndex = Backbone.View.extend({
  initialize: function () {
    var renderCallback = this.render.bind(this);
    this.listenTo(this.collection, "remove", renderCallback);
    this.listenTo(this.collection, "add", renderCallback);
    this.listenTo(this.collection, "change:title", renderCallback);
    this.listenTo(this.collection, "reset", renderCallback);
  },

  render: function () {
    var renderedContent = JST["posts/index"]({ /// this gets compiled to html
      posts: this.collection
    });

    this.$el.html(renderedContent);
    return this;
  },


  events: {
    "click .deleteButton": "deletePost",
    "click .postShowLink": "showPost"
  },

  deletePost: function (event) {
   var elemToDeleteID = parseInt(event.currentTarget.parentElement.id);
   var modelToDelete = this.collection.findWhere({id: elemToDeleteID});
   //console.log(typeof(elemToDeleteID));
   modelToDelete.destroy();
  },

  showPost: function (event) {
    event.preventDefault();
    var postToShowID = parseInt(event.currentTarget.parentElement.id);
    console.log(postToShowID);
    var postToShow = this.collection.findWhere({id: postToShowID});
    console.log(postToShow);
    var postView = new Journal.Views.PostShow({model: postToShow});
    console.log(postView);
    Journal.$el.empty();
    Journal.$el.append(postView.render().$el);
  }
});