Journal.Routers.PostsRouter = Backbone.Router.extend({
  routes: {
    "": "index",
    "posts/:id": "postShow"
  },

  index: function () {
    var postsView = new Journal.Views.PostsIndex({collection: Journal.posts});
    Journal.$el.append(postsView.render().$el);
  },

  postShow: function (id) {
    console.log("I AIN'T GETTING CALLED")
    var postToShow = this.collection.findWhere({id: id});
    var postShow = new Journal.Views.PostShow({post: postToShow});
    Journal.$el.empty();
    Journal.$el.append(postShow.render().$el);
  }
});