window.Journal = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    $.ajax({
      url: "/posts",
      type: "GET",
      dataType: 'json',
      success: function(resp){
        window.Journal.posts = new Journal.Collections.Posts(resp);
        window.Journal.$el = $('#content');
        // console.log(posts);
        var postsRouter = new Journal.Routers.PostsRouter();
        Backbone.history.start();
      }
    });

  }
};

$(document).ready(function(){
  Journal.initialize();
});