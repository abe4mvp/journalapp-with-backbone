class PostsController < ApplicationController
  def index
    puts "in the controller"
    @posts = Post.all

    respond_to do |format|
      format.html
      format.json {render json: @posts}
    end
  end

  def create
    @post = Post.new(params[:post])

    if @post.save
      render json: @post
    else
      render json: @post.errors.full_messages, status: 422
    end
  end

  def destroy
    @post = Post.find(params[:id])
     if @post && @post.destroy
       head :ok
     else
       render status: 422
     end
  end
end
